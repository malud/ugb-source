using UnityEngine;
using System.Collections;

namespace UGB.XUI
{
/// <summary>
/// each object which inherit this interface can be used in BaseScreen class per T GetWidget
/// </summary>
	public interface IWidget
	{
	}
}
